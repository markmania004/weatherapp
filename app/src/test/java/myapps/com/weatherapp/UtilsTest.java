package myapps.com.weatherapp;

import android.test.suitebuilder.annotation.SmallTest;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/**
 * Created by mching on 26/07/2018.
 */
@SmallTest
public class UtilsTest {

    private static final double DELTA = 0.01;

    @Test
    public void getAverageTemp() throws Exception {
        assertEquals("Average temp computation error", 30.565, Utils.getAverageTemp(28.46, 32.67));
        assertEquals("Average temp computation error", 27.5, Utils.getAverageTemp(25, 30));
        assertEquals("Average temp computation error", 28.775, Utils.getAverageTemp(27.55, 30));
    }

    @Test
    public void fahrenheitToCelsius() throws Exception {
        assertEquals("Conversion from celsius to fahrenheit failed", 0, Utils.fahrenheitToCelsius(32), DELTA);
        assertEquals("Conversion from celsius to fahrenheit failed", 82.22, Utils.fahrenheitToCelsius(180), DELTA);
        assertEquals("Conversion from celsius to fahrenheit failed", 93.33, Utils.fahrenheitToCelsius(200), DELTA);
    }

    @Test
    public void unixToDate() throws Exception {
        assertEquals("Conversion from epoch time to formatted date failed", "July 26", Utils.unixToDate("1532573225"));
        assertEquals("Conversion from epoch time to formatted date failed", "August 30", Utils.unixToDate("1535574000"));
        assertEquals("Conversion from epoch time to formatted date failed", "September 16", Utils.unixToDate("1505574000"));
    }
}