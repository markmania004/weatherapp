package myapps.com.weatherapp;

import android.test.suitebuilder.annotation.SmallTest;

import org.junit.Test;

import java.util.List;

import myapps.com.weatherapp.data.model.WeatherData;
import myapps.com.weatherapp.main.MainActivityContract;
import myapps.com.weatherapp.main.MainPresenter;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;

/**
 * Created by mching on 26/07/2018.
 */

@SmallTest
public class PresenterTest {

    @Test
    public void onNoInternet() throws Exception {
        MainPresenter presenter = new MainPresenter(new MainActivityContract.View() {
            @Override
            public void setRefreshing(boolean refresh) {}

            @Override
            public void showData(List<WeatherData> weatherDataList) {
                assertNull(weatherDataList);
            }

            @Override
            public void showDataFailed(String message) {
                assertEquals("Presenter no_net displays no internet failed", "No Internet Connection", message);
            }

            @Override
            public void showHeaderData(WeatherData data) {
                assertNull("Presenter no_net shows null header failed", data);
            }

            @Override
            public boolean isNetworkConnected() {
                return false;
            }
        }, null);
        presenter.loadData();
    }
}
