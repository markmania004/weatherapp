package myapps.com.weatherapp;

import android.app.Application;

import myapps.com.weatherapp.data.AppDataManager;
import myapps.com.weatherapp.data.network.AppApiHelper;

/**
 * Created by mching on 25/07/2018.
 */

public class WeatherApp extends Application {

    private AppDataManager appDataManager;

    @Override
    public void onCreate() {
        super.onCreate();
        AppApiHelper appApiHelper = new AppApiHelper();
        appDataManager = new AppDataManager(appApiHelper);
    }

    public AppDataManager getAppDataManager() {
        return this.appDataManager;
    }
}
