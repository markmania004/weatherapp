package myapps.com.weatherapp.splash;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import myapps.com.weatherapp.BuildConfig;
import myapps.com.weatherapp.R;
import myapps.com.weatherapp.databinding.ActivitySplashBinding;
import myapps.com.weatherapp.main.MainActivity;

/**
 * Created by mching on 25/07/2018.
 */

public class SplashActivity extends AppCompatActivity {

    ActivitySplashBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            }
        }, BuildConfig.SPLASH_DURATION);
    }

}
