package myapps.com.weatherapp.main;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import myapps.com.weatherapp.R;
import myapps.com.weatherapp.Utils;
import myapps.com.weatherapp.WeatherApp;
import myapps.com.weatherapp.data.model.WeatherData;
import myapps.com.weatherapp.databinding.ActivityMainBinding;

/**
 * Created by mching on 25/07/2018.
 */

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, MainActivityContract.View {

    private static final String TAG = MainActivity.class.getSimpleName();

    private ActivityMainBinding binding;
    private MainActivityContract.Presenter presenter;

    private WeatherAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        initialize();
    }

    private void initialize() {
        adapter = new WeatherAdapter(this, new ArrayList<WeatherData>());
        binding.recyclerview.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerview.setHasFixedSize(true);
        binding.recyclerview.setAdapter(adapter);
        binding.refreshlayout.setOnRefreshListener(this);
        presenter = new MainPresenter(this, ((WeatherApp) getApplication()).getAppDataManager());
        presenter.loadData();
    }

    @Override
    public void onRefresh() {
        presenter.loadData();
    }

    @Override
    public void setRefreshing(boolean refresh) {
        binding.refreshlayout.setRefreshing(refresh);
    }

    @Override
    public void showData(List<WeatherData> weatherDataList) {
        Log.d(TAG, "showing data... " + weatherDataList.size() + " items.");
        adapter = new WeatherAdapter(MainActivity.this, weatherDataList);
        binding.recyclerview.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void showDataFailed(String message) {
        Utils.getErrorDialog(MainActivity.this, message).show();
    }

    @Override
    public void showHeaderData(WeatherData data) {
        binding.tvWeather.setText(data.getWeather());
        binding.tvTempRange.setText(Utils.getTempRange(this, data.getMinTemp(), data.getMaxTemp()));
        binding.tvTemp.setText(Utils.appendDegrees(this, Integer.toString((int) Utils.getAverageTemp(data.getMinTemp(), data.getMaxTemp()))));
        binding.tvSpeed.setText(Utils.unixToDate(data.getEpochDate()));
    }

    @Override
    public boolean isNetworkConnected() {
        return Utils.isNetworkActive(MainActivity.this);
    }
}
