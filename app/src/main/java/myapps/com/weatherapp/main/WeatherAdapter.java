package myapps.com.weatherapp.main;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import myapps.com.weatherapp.R;
import myapps.com.weatherapp.Utils;
import myapps.com.weatherapp.data.model.WeatherData;

/**
 * Created by mching on 25/07/2018.
 */

public class WeatherAdapter extends RecyclerView.Adapter<WeatherAdapter.WeatherDataViewHolder> {

    private List<WeatherData> list;
    private Context context;

    public WeatherAdapter(Context context, List<WeatherData> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public WeatherDataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_weather, parent,false);
        return new WeatherDataViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull WeatherDataViewHolder holder, int position) {
        final WeatherData data = list.get(position);
        holder.tvTemp.setText(Utils.appendDegrees(context, Integer.toString((int)Utils.getAverageTemp(data.getMinTemp(), data.getMaxTemp()))));
        holder.tvDay.setText(position == 0 ? context.getString(R.string.label_tomorrow) : Utils.unixToDate(data.getEpochDate()));
        holder.tvWeather.setText(data.getWeather());
        holder.tvTempRange.setText(Utils.getTempRange(context, data.getMinTemp(), data.getMaxTemp()));
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(data.getMobileLink()));
                    context.startActivity(myIntent);
                } catch (ActivityNotFoundException e) {
                    Toast.makeText(context, "No application can handle this request."
                            + " Please install a webbrowser",  Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class WeatherDataViewHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView tvTemp;
        TextView tvDay;
        TextView tvWeather;
        TextView tvTempRange;

        public WeatherDataViewHolder(View itemView) {
            super(itemView);
            tvTemp = itemView.findViewById(R.id.cardTemp);
            tvDay = itemView.findViewById(R.id.cardDay);
            tvWeather = itemView.findViewById(R.id.cardWeather);
            tvTempRange = itemView.findViewById(R.id.cardTempRange);
            cardView = itemView.findViewById(R.id.cardview);
        }
    }
}
