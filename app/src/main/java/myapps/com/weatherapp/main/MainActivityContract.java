package myapps.com.weatherapp.main;

import java.util.List;

import myapps.com.weatherapp.data.model.WeatherData;

/**
 * Created by mching on 25/07/2018.
 */

public interface MainActivityContract {

    interface View {
        void setRefreshing(boolean refresh);
        void showData(List<WeatherData> weatherDataList);
        void showDataFailed(String message);
        void showHeaderData(WeatherData data);
        boolean isNetworkConnected();
    }

    interface Presenter {
        void loadData();
    }
}
