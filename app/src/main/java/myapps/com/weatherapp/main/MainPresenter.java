package myapps.com.weatherapp.main;

import android.util.Log;

import myapps.com.weatherapp.data.AppDataManager;
import myapps.com.weatherapp.data.model.WeatherDataResponse;
import myapps.com.weatherapp.data.network.ApiHelper;

/**
 * Created by mching on 25/07/2018.
 */

public class MainPresenter implements MainActivityContract.Presenter {

    private static final String TAG = MainPresenter.class.getSimpleName();

    private MainActivityContract.View view;
    private AppDataManager appDataManager;

    public MainPresenter(MainActivityContract.View view, AppDataManager appDataManager) {
        this.view = view;
        this.appDataManager = appDataManager;
    }

    @Override
    public void loadData() {
        if(view.isNetworkConnected()) {
            view.setRefreshing(true);
            appDataManager.fetchWeatherForecast(new ApiHelper.ApiInteractor() {
                @Override
                public void onFinished(WeatherDataResponse weatherDataResponse) {
                    Log.d(TAG, "on finished success");
                    //  Removes first item of the result, then display it as header
                    view.showHeaderData(weatherDataResponse.getDataList().remove(0));
                    view.showData(weatherDataResponse.getDataList());
                    view.setRefreshing(false);
                }

                @Override
                public void onFailure() {
                    Log.d(TAG, "on fail");
                    view.setRefreshing(false);
                }
            });
        } else {
            view.showDataFailed("No Internet Connection");
            view.setRefreshing(false);
        }
    }
}
