package myapps.com.weatherapp;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mching on 25/07/2018.
 */

public class Utils {

    private static final String DECIMAL_FORMAT      = "#.##";
    private static final String DATE_FORMAT         = "MMMM dd";

    public static double getAverageTemp(double min, double max) {
        return (min + max) / 2;
    }

    public static double fahrenheitToCelsius(double fahrenheit) {
        return ((fahrenheit - 32) * 5) / 9;
    }

    public static String appendDegrees(Context context, String temp) {
        return context.getString(R.string.label_degrees, temp);
    }

    public static String twoDecimalPlaces(double number) {
        DecimalFormat decimalFormat = new DecimalFormat(DECIMAL_FORMAT);
        return decimalFormat.format(number);
    }

    public static String getTempRange(Context context, double min, double max) {
        return appendDegrees(context, Integer.toString((int)min)) + " - " + appendDegrees(context, Integer.toString((int)max));
    }

    public static String unixToDate(String dt) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DATE_FORMAT);
        return simpleDateFormat.format(new Date(Long.valueOf(dt) * 1000L));
    }

    public static boolean isNetworkActive(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnected();
    }

    public static AlertDialog.Builder getErrorDialog(Context context, String message) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(message);
        builder.setMessage(context.getString(R.string.error_message));
        builder.setPositiveButton(context.getString(R.string.label_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {}
        });
        return builder;
    }
}
