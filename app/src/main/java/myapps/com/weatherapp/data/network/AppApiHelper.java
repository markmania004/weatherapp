package myapps.com.weatherapp.data.network;

/**
 * Created by mching on 25/07/2018.
 */

public class AppApiHelper implements ApiHelper {

    private static final String TAG             = AppApiHelper.class.getSimpleName();
    private static final String TAGUIG_ID       = "759349";

    @Override
    public void fetchWeatherForecast(final ApiInteractor apiInteractor) {
        FetchWeatherDataTask fetchWeatherDataTask = new FetchWeatherDataTask(apiInteractor, TAGUIG_ID);
        fetchWeatherDataTask.execute();
    }
}
