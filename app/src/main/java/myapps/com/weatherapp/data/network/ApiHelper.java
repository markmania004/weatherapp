package myapps.com.weatherapp.data.network;

import myapps.com.weatherapp.data.model.WeatherDataResponse;

/**
 * Created by mching on 25/07/2018.
 */

public interface ApiHelper {

    void fetchWeatherForecast(ApiInteractor apiInteractor);

    interface ApiInteractor {
        void onFinished(WeatherDataResponse weatherDataResponse);
        void onFailure();
    }
}
