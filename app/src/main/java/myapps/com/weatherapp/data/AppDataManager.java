package myapps.com.weatherapp.data;

import myapps.com.weatherapp.data.network.ApiHelper;

/**
 * Created by mching on 25/07/2018.
 */

public class AppDataManager implements ApiHelper {

    private final ApiHelper apiHelper;

    public AppDataManager(ApiHelper apiHelper) {
        this.apiHelper = apiHelper;
    }

    @Override
    public void fetchWeatherForecast(ApiInteractor apiInteractor) {
        apiHelper.fetchWeatherForecast(apiInteractor);
    }
}
