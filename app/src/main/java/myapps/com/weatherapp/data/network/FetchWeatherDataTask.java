package myapps.com.weatherapp.data.network;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import myapps.com.weatherapp.BuildConfig;
import myapps.com.weatherapp.Utils;
import myapps.com.weatherapp.data.model.WeatherDataResponse;
import myapps.com.weatherapp.data.model.WeatherData;

/**
 * Created by mching on 25/07/2018.
 */

public class FetchWeatherDataTask extends AsyncTask <Void, Void, WeatherDataResponse> {

    private String query = BuildConfig.WEATHER_HOST + "forecasts/v1/daily/5day/";

    private final ApiHelper.ApiInteractor apiInteractor;
    private final String cityId;

    public FetchWeatherDataTask(ApiHelper.ApiInteractor interactor, String cityId) {
        this.apiInteractor = interactor;
        this.cityId = cityId;
    }

    @Override
    protected WeatherDataResponse doInBackground(Void... voids) {
        BufferedReader reader = null;

        try {
            URL url = new URL(query + cityId + "?apikey=" + BuildConfig.WEATHER_API_KEY);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.connect();

            int statusCode = conn.getResponseCode();

            if (statusCode != 200) {
                return null;
            }

            InputStream inputStream = conn.getInputStream();
            StringBuilder buffer = new StringBuilder();

            if (inputStream == null) {
                return null;
            }
            reader = new BufferedReader(new InputStreamReader(inputStream));

            String line;
            while ((line = reader.readLine()) != null) {
                buffer.append(line).append("\n");
            }

            if (buffer.length() == 0) {
                return null;
            }

            String response = buffer.toString();

            return parseResponse(response);

        } catch (IOException e) {

            e.printStackTrace();
            return null;

        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private WeatherDataResponse parseResponse(String response) {
        WeatherDataResponse weatherDataResponse = new WeatherDataResponse();
        try {

            JSONObject responseJson = new JSONObject(response);
            JSONObject headline = responseJson.getJSONObject("Headline");
            JSONArray dataJson = responseJson.getJSONArray("DailyForecasts");

            //  Parse through 'headline'
            weatherDataResponse.setEffectiveEpochDate(headline.getString("EffectiveEpochDate"));
            weatherDataResponse.setText(headline.getString("Text"));
            weatherDataResponse.setCategory(headline.getString("Category"));
            weatherDataResponse.setEndEpochDate(headline.getString("EndEpochDate"));
            weatherDataResponse.setMobileLink(headline.getString("MobileLink"));
            weatherDataResponse.setLink(headline.getString("Link"));

            //  Parse through 'DailyForecasts'
            List<WeatherData> data = new ArrayList<>();
            for(int i = 0; i < dataJson.length(); i++) {
                JSONObject jsonObject = dataJson.getJSONObject(i);
                JSONObject jsonTemp = jsonObject.getJSONObject("Temperature");
                WeatherData weatherData = new WeatherData();
                weatherData.setEpochDate(jsonObject.getString("EpochDate"));
                weatherData.setMinTemp(Utils.fahrenheitToCelsius(jsonTemp.getJSONObject("Minimum").getDouble("Value")));
                weatherData.setMaxTemp(Utils.fahrenheitToCelsius(jsonTemp.getJSONObject("Maximum").getDouble("Value")));
                weatherData.setWeather(jsonObject.getJSONObject("Day").getString("IconPhrase"));
                weatherData.setLink(jsonObject.getString("Link"));
                weatherData.setMobileLink(jsonObject.getString("MobileLink"));
                data.add(weatherData);
            }

            weatherDataResponse.setDataList(data);

        } catch (JSONException e){
            e.printStackTrace();
            apiInteractor.onFailure();
        }

        return weatherDataResponse;
    }

    @Override
    protected void onPostExecute(WeatherDataResponse weatherDataResponse) {
        super.onPostExecute(weatherDataResponse);

        if(weatherDataResponse != null) {
            apiInteractor.onFinished(weatherDataResponse);
        } else {
            apiInteractor.onFailure();
        }
    }
}
