package myapps.com.weatherapp.data.model;

import java.util.List;

/**
 * Created by mching on 25/07/2018.
 */

public class WeatherDataResponse {

    private List<WeatherData> dataList;
    private String effectiveEpochDate;
    private String endEpochDate;
    private String text;
    private String category;
    private String link;
    private String mobileLink;

    public List<WeatherData> getDataList() {
        return dataList;
    }

    public void setDataList(List<WeatherData> dataList) {
        this.dataList = dataList;
    }

    public String getEffectiveEpochDate() {
        return effectiveEpochDate;
    }

    public void setEffectiveEpochDate(String effectiveEpochDate) {
        this.effectiveEpochDate = effectiveEpochDate;
    }

    public String getEndEpochDate() {
        return endEpochDate;
    }

    public void setEndEpochDate(String endEoochDate) {
        this.endEpochDate = endEoochDate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getMobileLink() {
        return mobileLink;
    }

    public void setMobileLink(String mobileLink) {
        this.mobileLink = mobileLink;
    }
}
