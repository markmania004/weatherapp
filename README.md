# Weather Demo App

I chose to make the weather app instead of the RSS feed.

## App Behavior

1. Splash screen on open.
2. Internet is checked before loading data.
3. If no internet, error is shown.
4. If internet is connected, loads data from Accuweather.
5. Pull to refresh.
6. On click, user is directed to Accuweather site for more details.

## Notes

* Only 5 days of weather forecasts shown because Accuweather & Openweather FREE account API keys are limited to only 5 days of daily forecasts.
	I needed to apply for a paid account to get all 10 days.
* App built based on MVP architecture.
* No local saving due to the time constraint.
* No third party libraries.
* Location fetch for weather is static using TAGUIG location_id, although query is perfectly fine handling other locations provided the location_id.
